FROM ubuntu:14.04

RUN apt-get update && \
    apt-get install -y nginx && \
    rm -rf /var/lib/apt/lists/*

RUN rm -f /etc/nginx/sites-enabled/*
COPY nginx.conf /etc/nginx/
COPY nginx_server.conf /etc/nginx/sites-enabled/
COPY index.html /nginx/html/

EXPOSE 80

CMD /usr/sbin/nginx
